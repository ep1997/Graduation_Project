﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scenemanager : MonoBehaviour
{
    public GameObject Handmonster;
    public GameObject HandTrigger;
    public GameObject HandOutTrigger;

    public GameObject Klaue;
    public GameObject Cara;

    public GameObject DarkCara;
    public GameObject RedCara;


	// Use this for initialization
	void Start ()
    {
		if(SaveGame.savegame.angeredCara == true)
        {
            Handmonster.GetComponent<Animator>().SetBool("Die", true);
            HandTrigger.SetActive(false);
            HandOutTrigger.SetActive(false);

            Klaue.GetComponent<Rigidbody2D>().simulated = true;
            //Cara.SetActive(false);
            DarkCara.SetActive(true);
            RedCara.SetActive(true);

        }
        else
        {
            Debug.Log("not yet, master");
        }
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
