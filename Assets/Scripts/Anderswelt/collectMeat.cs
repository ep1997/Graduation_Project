﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class collectMeat : MonoBehaviour
{
    public GameObject meat_In_Mouth;

    void OnTriggerEnter(Collider col)
    {
        if (col.gameObject.name == "Cube")
        {
            if (gameObject.name == "meatTrigger")
            {
                SaveGame.savegame.collectedMeat = true;
                SaveGame.savegame.Save();
                meat_In_Mouth.SetActive(true);
            }
            else if (gameObject.name == "scissorTrigger" /*&& SaveGame.savegame.gotScissors == true*/)
            {
                Debug.Log("cut rope");
                SaveGame.savegame.angeredCara = true;
                SaveGame.savegame.Save();
                meat_In_Mouth.GetComponent<Rigidbody2D>().simulated = true;

            }



        }
    }
}
