﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class timing : collectMeat
{
    float timer = 60;
	// Use this for initialization
	void Start ()
    {
		
	}
	
	// Update is called once per frame
	void Update ()
    {
        timer -= Time.deltaTime;

        if(timer == 0)
        {
            SceneManager.LoadScene("Level");
        }

        if(timer == 0 && SaveGame.savegame.collectedMeat == true)
        {
            SceneManager.LoadScene("Level_Meat");
        }
	}
}
