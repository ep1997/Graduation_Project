﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class WakeUp : MonoBehaviour
{
    public GameObject meat;
	// Use this for initialization
	void Start ()
    {
		
	}
	
	// Update is called once per frame
	void Update ()
    {
		if(Input.GetKeyDown(KeyCode.Alpha1) && meat.activeInHierarchy == false)
        {
            SceneManager.LoadScene("Level 1");
        }

        if (Input.GetKeyDown(KeyCode.Alpha1) && meat.activeInHierarchy == true)
        {
            SceneManager.LoadScene("Level 1");
        }
    }
}
