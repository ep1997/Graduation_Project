﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Geweihscript : MonoBehaviour
{
    public Transform target;
    public Transform origin;
    public Transform Playerpos;
    public float speed;
    Animation anim;
    public GameObject Player;
    GameObject childGO;
    public bool grounded = false;
    public float timer = 3;

    // Use this for initialization
    void Start ()
    {
        anim = GetComponent<Animation>();
        childGO = transform.FindChild("geweih").gameObject;
        
    }
	
	// Update is called once per frame
	void Update ()
    {

        float stepp = speed * Time.deltaTime;
        transform.position = Vector3.MoveTowards(transform.position, Playerpos.position, stepp);

        if ((Vector3.Distance(Player.transform.position, transform.position) > 3 && Input.GetKeyDown(KeyCode.H)))
        {
            Player.transform.parent = GameObject.Find("Monster").transform.GetChild(2);
            print("HOLD ON");
        }

        if ((Vector3.Distance(childGO.transform.position, target.transform.position) < 1)) 
        {
            grounded = true;
        }

        if ((Vector3.Distance(childGO.transform.position, origin.transform.position) < 1))
        {
            grounded = false;
        }

        if (Vector3.Distance(Player.transform.position, transform.position) <= 4) //animation not found - legacy problem
        {
            anim.CrossFade("attack_claw", 1.5f);
        }

        if (Vector3.Distance(Player.transform.position, transform.position) > 5 && grounded == false)
        {
            float step = speed * Time.deltaTime;
            childGO.transform.position = Vector3.MoveTowards(childGO.transform.position, target.position, step);

            
        }

        if(grounded == true) 
        {


            timer -= Time.deltaTime;

            if (timer <= 0)
            {
                float step = speed * Time.deltaTime;
                childGO.transform.position = Vector3.MoveTowards(childGO.transform.position, origin.position, step);
                //timer = 2;
            }
        }

        if(grounded == false)
        {
            timer = 3;
        }

    }

    //private void OnTriggerEnter2D(Collider2D col)
    //{
    //    if((Vector3.Distance(Player.transform.position, transform.position) < 50 && Input.GetKeyDown(KeyCode.H)))
    //    {
    //        Player.transform.parent = GameObject.Find("Monster").transform;
    //    }
    //}
}
