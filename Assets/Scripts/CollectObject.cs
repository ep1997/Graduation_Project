﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CollectObject : ClickMe
{
    public Transform collect;
    public GameObject branches;
    Sprite Flash;
    public GameObject writing;
    //public GameObject Flash;
    void Start()
    {
        //Flash = Resources.Load<GameObject>("Flash");
    }




    void OnTriggerEnter(Collider col)
    {
        if (col.gameObject.name == "Cube")
        {
            transform.position = collect.transform.position;
            print("Collect");
            camera_active = true;
            transform.parent = Player.transform;
            writing.SetActive(true);
        }



        if (col.gameObject.name == "Eyemonster" && Input.GetKeyDown(KeyCode.F))
        {
            SceneManager.LoadScene("Anderswelt");
        }
    }

    void OnTriggerStay(Collider co)
    {
        if (co.gameObject.name == "Eyemonster" && Input.GetKeyDown(KeyCode.F) && branches.activeInHierarchy == false)
        {
            print("LOAD");
            Instantiate(Resources.Load<GameObject>("Flash"));
            SceneManager.LoadScene("Anderswelt");
        }

        if (co.gameObject.name == "Eyemonster" && Input.GetKeyDown(KeyCode.F) && branches.activeInHierarchy == true)
        {
            print("LOAD");
            Instantiate(Resources.Load<GameObject>("Flash"));
            SceneManager.LoadScene("Anderswelt 1");
        }
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.F))
        {
            print("CLICK");
            //make camera noise
            //play flash anim
            //load upsidedown scene
        }

    }

}
