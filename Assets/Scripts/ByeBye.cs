﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ByeBye : MonoBehaviour
{
    public void DeleteYourself()
    {
        Destroy(gameObject);
    }

}
