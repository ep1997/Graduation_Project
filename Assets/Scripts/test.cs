﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class test : MonoBehaviour
{

	// Use this for initialization
	void Start ()
    {
        Screen.SetResolution(1024, 360, true);
    }
	
	// Update is called once per frame
	void Update ()
    {
        if (Screen.fullScreen || Camera.main.aspect != 1)
        {
            Screen.SetResolution(1024, 360, true);
        }
    }
}
