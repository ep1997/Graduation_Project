﻿using UnityEngine;
using System.Collections;
using System;

public class CharController : MonoBehaviour
{
    public float Maxspeed = 2;
    public float jumpForce = 15;
    public bool grounded = false;
    public Rigidbody2D rigi;
    bool candoublejump = false;

	void Update ()
    {
        var motion = new Vector3(Input.GetAxis("Horizontal"), 0, 0);
        transform.position += motion * Maxspeed * Time.deltaTime;


        if (transform.position.y <= 0)
        {
            grounded = true;
        }

        RotateCharacter();
        JumpUp();
    }

    public void JumpUp()
    {

        if (Input.GetKeyDown(KeyCode.Space))
        {
            if(grounded == true)
            {
                rigi.velocity = new Vector2(rigi.velocity.x, 0);
                rigi.AddForce(new Vector2(0, jumpForce));
                candoublejump = true;
                grounded = false;
            }
            else
            {
                if(candoublejump == true)
                {
                    candoublejump = false;
                    rigi.velocity = new Vector2(rigi.velocity.x, 0);
                    rigi.AddForce(new Vector2(0, jumpForce));
                }
            }


        }
    }

    private void RotateCharacter()
    {
        if (Input.GetKeyDown(KeyCode.A) || (Input.GetKeyDown(KeyCode.LeftArrow)))
        {
            transform.eulerAngles = new Vector3(0, 0, 0);
        }

        if (Input.GetKeyDown(KeyCode.D) || (Input.GetKeyDown(KeyCode.RightArrow)))
        {
            transform.eulerAngles = new Vector3(0, 180, 0);
        }

    }
}

