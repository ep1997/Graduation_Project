﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Dialogue1 : ClickMe
{
    public Text textComponent;

    public string[] Dialogues;
    public string[] SecondDialogues;

    public float TimeBetweenTexts = 0.15f;
    public float TextSpeedBoost = 0.15f;

    public KeyCode DisplayDialogue = KeyCode.Return;

    public bool _isStringAlreadyRevealed = false;
    bool _isDialoguePlaying = false;
    public bool _isEndOfDialogue = false;

    public GameObject ContinueIcon;
    public GameObject StopIcon;

    public GameObject MyPlane;
    public GameObject Char;

    public int talkCount = 0;

    public GameObject DialogueBox;


    void Start()
    {
        textComponent.text = "";
        DialogueBox = GameObject.FindGameObjectWithTag("DialogueBox");

        ContinueIcon = GameObject.FindGameObjectWithTag("Continue");
        StopIcon = GameObject.FindGameObjectWithTag("Stop");
    }

    void Update()
    {
        if(_isEndOfDialogue && Input.GetKeyDown(KeyCode.Return))
        {
            DialogueBox.SetActive(false);
            _isEndOfDialogue = false;
        }
    }

    public void Conversation()
    {
                _isDialoguePlaying = true;
                StartCoroutine(StartDialogue());
                //Scripts on/off
                MyPlane.GetComponent<PointnClick1>().enabled = false;
                Char.GetComponent<CharController>().enabled = false;
    }

    public int currentDialogueIndex; //a variable that counts on wich part of the array we are currently, for the beginning on the very first: 0
    //When clicking out of the dialogue before its finished
    public void ResetDialogue()
    {
        if(_isDialoguePlaying == true)
        {
            currentDialogueIndex = 0;
            _isStringAlreadyRevealed = false;
            _isDialoguePlaying = false;
        }
        else
        {
            return;
        }
    }

    IEnumerator StartDialogue() 
    {
            if(talkCount == 0) //In case The player hasnt spoke with the npc before
        {
            int dialogueLength = Dialogues.Length; //a variable that stands for how many strings are in this array
            currentDialogueIndex = 0;
            while (currentDialogueIndex < dialogueLength || !_isStringAlreadyRevealed) //while the part we are currently on isnt the last or the text hasnt even begun to play
            {
                if (!_isStringAlreadyRevealed) //if text hasnt been played yet
                {
                    _isStringAlreadyRevealed = true; //now it begins to play
                    StartCoroutine(DisplayText(Dialogues[currentDialogueIndex++])); //so I am starting a coroutine within thats focused on actually making the text appear

                    if (currentDialogueIndex >= dialogueLength) //When the current position in the array equals with the number of strings
                    {
                        _isEndOfDialogue = true; //then the dialogue is over
                    }
                }

                yield return 0; //return here on the next frame
            }

            while (true)
            {
                if (Input.GetKeyDown(DisplayDialogue)) //If the return key is pushed
                {
                    break; //break out of the loop
                }

                yield return 0; //return here on the next frame
            }

/*            HideIcons();*/ //Hide the green and red arrow image
            _isEndOfDialogue = false;
            _isDialoguePlaying = false;
        }
            else
        {
                if (talkCount > 0) //In case the player already spoke to this npc, basically the same spiel just with another array
            {
              

                int dialogueLength = SecondDialogues.Length;
                int currentDialogueIndex = 0;
                while (currentDialogueIndex < dialogueLength || !_isStringAlreadyRevealed)
                {
                    if (!_isStringAlreadyRevealed)
                    {
                        _isStringAlreadyRevealed = true;
                        StartCoroutine(DisplayText(SecondDialogues[currentDialogueIndex++]));

                        if (currentDialogueIndex >= dialogueLength)
                        {
                            _isEndOfDialogue = true;
                        }
                    }

                    yield return 0;
                }

                while (true)
                {
                    if (Input.GetKeyDown(DisplayDialogue))
                    {
                        break;
                    }

                    yield return 0;
                }

                //HideIcons();
                _isEndOfDialogue = false;
                _isDialoguePlaying = false;
            }
        }



    }

    IEnumerator DisplayText(string stringsToDisplay)
    {
        int arrayLength = stringsToDisplay.Length; //Lenght of the Text
        int currentDialogueIndex = 0; //To track witch text fragment is up to me displayed. "0" will be the first one.

        //HideIcons();

        textComponent.text = ""; //Whenever this coroutine is permormed, the text will be cleared to make space.

        while (currentDialogueIndex < arrayLength) //while not all text pieces have been played yet
        {
            textComponent.text += stringsToDisplay[currentDialogueIndex]; // count with every text piece shown
            currentDialogueIndex++;

            if (currentDialogueIndex < arrayLength) //while not all text pieces have been played yet
            {
                if (Input.GetKey(DisplayDialogue)) //If the return key is being held down
                {
                    yield return new WaitForSeconds(TimeBetweenTexts * TextSpeedBoost); //display text but faster
                }
                else //If the Return key is pressed only once
                {
                    yield return new WaitForSeconds(TimeBetweenTexts); //display text with normal speed
                }
            }
            else //If the end of the index is arrived
            {
                break; //stop displaying the text
            }
        }

        ShowIcons();

        while (true)
        {
            if (Input.GetKeyDown(DisplayDialogue))
            {
                break;
            }

            yield return 0;
        }

        _isStringAlreadyRevealed = false;
        textComponent.text = "";
    }

    public GameObject Scissors;
    private void ShowIcons()
    {
        if (_isEndOfDialogue)
        {
            if(gameObject.name == "Eyemonster_Text")
            {
                Debug.Log("Monster");
                SceneManager.LoadScene("Anderswelt");
            }
            if (gameObject.name == "Believer_Reward")
            {
                Debug.Log("Reward");
                SaveGame.savegame.gotScissors = true;
                SaveGame.savegame.Save();
                Scissors.SetActive(true);
            }

            //DialogueBox.SetActive(false);
            MyPlane.GetComponent<PointnClick1>().enabled = true;
            Char.GetComponent<CharController>().enabled = true;
            talkCount += 1;

        }


        ContinueIcon.SetActive(true);
    }
}

