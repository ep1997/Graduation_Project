﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ClickMe : MonoBehaviour
{
    public GameObject textToShow;
    public GameObject textToShowForReward;
    public int triggerCount = 0;
    public GameObject texty;
    public bool camera_active = false;
    public GameObject Player;
    public GameObject[] Texts;
    // Use this for initialization
    void Start ()
    {
        Player = GameObject.FindGameObjectWithTag("Hit");
	}
	

    public void OnTriggerEnter(Collider col)
    {
        if(col.gameObject.name == "Cube")
        {
            if(gameObject.name == "Picture" && SaveGame.savegame.collectedMeat == true)
            {
                triggerCount += 1;
                texty.SetActive(true);
                textToShowForReward.SetActive(true);
                textToShowForReward.GetComponent<Dialogue1>().enabled = true;
                textToShowForReward.GetComponent<Dialogue1>().Conversation();
            }
            else
            {
                triggerCount += 1;
                texty.SetActive(true);
                textToShow.SetActive(true);
                textToShow.GetComponent<Dialogue1>().enabled = true;
                textToShow.GetComponent<Dialogue1>().Conversation();
            }

        }

    }

    public void ShowSpeakText()
    {

    }

    public void ShowSeeText()
    {
        //Same as Speak text basically, but portayed as monologue
    }


    void OnTriggerExit(Collider col)
    {
        if (col.gameObject.name == "Cube")
        {
            texty.SetActive(false);
            textToShow.SetActive(false);
            Player.GetComponent<CharController>().enabled = true;

            textToShow.GetComponent<Dialogue1>().ResetDialogue();
            textToShow.GetComponent<Dialogue1>().enabled = false;
        }

    }
}
