﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChaseScript : MonoBehaviour
{
    Vector3 PlayerPos;
    Vector3 EndPos;
   
    public GameObject Player;
    public GameObject cubey;
    public GameObject Hand;
    float thisSpeed = 5;
    bool continueto = true;
    public bool init = false;

    public Animator handAnimator;

    void Start()
    {
       
    }

    void OnTriggerEnter2D(Collider2D col)
    {
        if(col.gameObject.name == "Player" && continueto == true)
        {
            init = true;
            handAnimator.SetBool("Near_enough", true);
            Player.transform.parent = Hand.transform;
           
        }
    }
	
	void Update ()
    {
        if (Vector3.Distance(Player.transform.position, transform.position) < 50 && init == false)
        {
            
            PlayerPos = new Vector3(Player.transform.position.x, transform.position.y, Player.transform.position.z);
            transform.position = Vector3.MoveTowards(transform.position, PlayerPos, thisSpeed * Time.deltaTime);
            //transform.LookAt(Player);
        }


        if (Vector3.Distance(Player.transform.position, transform.position) < 20 && init == true)
        {
            EndPos = new Vector3(cubey.transform.position.x, transform.position.y, cubey.transform.position.z);
            transform.position = Vector3.MoveTowards(transform.position, EndPos, thisSpeed * Time.deltaTime);
        }

        if(transform.position == EndPos)
        {
            Player.transform.parent = null;
            init = false;
            Player.GetComponent<CharController>().enabled = true;
            Hand.GetComponent<ChaseScript>().enabled = false;
            handAnimator.SetBool("Near_enough", false);
        }
        return;
}
}
