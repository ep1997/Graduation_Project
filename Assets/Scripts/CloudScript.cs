﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CloudScript : MonoBehaviour
{
    bool instantiate_cloud = false;
    float my_speed = -1;
    void Start()
    {

    }

    void Update()
    {
        transform.position = new Vector3(transform.position.x + (my_speed * Time.deltaTime), 2.21f, 0);

        if (!instantiate_cloud)
        {
            if (transform.position.x <= 0)
            {
                instantiate_cloud = true;
                GameObject[] go = Resources.LoadAll<GameObject>("clouds");
                Instantiate(go[UnityEngine.Random.Range(0, go.Length - 1)], transform.FindChild("spawnit").position, Quaternion.identity);
            }
        }

        if (transform.position.x < -25)
        {
            Destroy(gameObject);
        }
    }
}
