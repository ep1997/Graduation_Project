﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InstantiatePos : MonoBehaviour
{
    public GameObject text_transform;
    // Use this for initialization
    void Start()
    {
        text_transform = GameObject.Find("Cube (3)");
        transform.position = text_transform.transform.position;
    }

    // Update is called once per frame
    void Update()
    {

    }

    void ByeBye()
    {
        Destroy(gameObject);
    }
}
