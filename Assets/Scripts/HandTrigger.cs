﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HandTrigger : MonoBehaviour
{
    public GameObject Hand;
    GameObject Player;
    public Transform SPAWN;    
	// Use this for initialization
	void Start ()
    {
        Player = GameObject.Find("Player");
	}

    void OnTriggerEnter2D(Collider2D col)
    {
        if(col.gameObject.name == "Player")
        {
            Hand.SetActive(true);
            Hand.GetComponent<ChaseScript>().enabled = true;
            Player.GetComponent<CharController>().enabled = false;
        }
    }
	
	// Update is called once per frame
	void Update ()
    {
		
	}
}
