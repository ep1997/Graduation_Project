﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class LifeManagement : MonoBehaviour
{

    GameObject HeartNormal;
    GameObject HeartWorse;
    GameObject HeartWorst;

    public GameObject cam;
    public Transform SPAWN;


    public int lifes = 2;

    public int GetSetLifes
    {
        get { return lifes; }
        set
        {
            lifes = value;
            SetHearts();
        }
    }

	void Start ()
    {

        HeartWorst = Resources.Load<GameObject>("1Life");
        HeartWorse = Resources.Load<GameObject>("2Life");
        HeartNormal = Resources.Load<GameObject>("3Life");

    }

    private void SetHearts()
    {
        switch (lifes)
        {
            case 0:
                SceneManager.LoadScene("death");
                break;
            case 1:
                HeartWorst = Instantiate(Resources.Load<GameObject>("1Life"), SPAWN.position, transform.rotation);
                HeartWorst.transform.parent = cam.transform;
                break;
            case 2:
                HeartWorse = Instantiate(Resources.Load<GameObject>("2Life"), SPAWN.position, transform.rotation);
                HeartWorse.transform.parent = cam.transform;
                break;
            case 3:
                print("CASE");
                Instantiate(Resources.Load<GameObject>("3Life"), SPAWN.position, SPAWN.rotation);
                //HeartNormal.transform.parent = cam.transform;
                //Instantiate(Resources.Load<GameObject>("fire_bullet"), spawn.position, spawn.rotation);
                break;
        }
    }

}
