﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Damage : MonoBehaviour
{
    public Image Fill_Life;
    public GameObject Manager;
    // Use this for initialization
    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.gameObject.tag == "enemy")
        {
            Fill_Life.fillAmount -= 0.25f;
        }

        if (Fill_Life.fillAmount == 0)
        {
            Manager.GetComponent<lifetest>().GetSetLifes -= 1;
        }

        if (Fill_Life.fillAmount <= 0)
        {
            Fill_Life.fillAmount += 1;
        }
    }
}
