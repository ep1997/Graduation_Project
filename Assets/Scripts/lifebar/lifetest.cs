﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class lifetest : MonoBehaviour
{

    Image Lifecount;
    public int Lifes = 4;
    Sprite oneLife;
    Sprite twoLife;
    Sprite threeLife;
    Sprite zeroLife;

    public int GetSetLifes
    {
        get { return Lifes; }
        set
        {
            Lifes = value;
            SetHearts();
        }
    }



    // Use this for initialization
    void Start()
    {
        Lifecount = GameObject.Find("Canvas").transform.FindChild("Lifepanel").GetChild(0).GetComponent<Image>();
        zeroLife = Resources.Load<Sprite>("0Life");
        oneLife = Resources.Load<Sprite>("1Life");
        twoLife = Resources.Load<Sprite>("2Life");
        threeLife = Resources.Load<Sprite>("3Life");
    }

    private void SetHearts()
    {
        switch (Lifes)
        {
            case 0:
                Lifecount.sprite = zeroLife;
                SceneManager.LoadScene("death");
                break;
            case 1:
                Lifecount.sprite = oneLife;
                break;
            case 2:
                Lifecount.sprite = twoLife;
                break;
            case 3:
                Lifecount.sprite = threeLife;
                break;

        }
    }

    // Update is called once per frame
    void Update()
    {

    }
}
