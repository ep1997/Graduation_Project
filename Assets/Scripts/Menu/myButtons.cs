﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class myButtons : MonoBehaviour
{

    // Use this for initialization
    public void Instructions()
    {
        SceneManager.LoadScene("Instructions");
        Debug.Log("HI");
    }

    public void Begin()
    {
        SceneManager.LoadScene("Level");
    }

    public void StopIt()
    {
        Application.Quit();
    }

    public void Back()
    {
        SceneManager.LoadScene("NewMenu");
    }
}
