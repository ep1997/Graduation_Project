﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

public class SaveGame : MonoBehaviour
{
    //static reference to this script, so I can easily access it from other methods
    public static SaveGame savegame;

    public bool collectedMeat;
    public bool gotScissors;
    public bool angeredCara;

    void Awake()
    {
        //Check if a savegame object already exists in the scene
        if (savegame == null)
        {
            //If it doesnt then this will be the savegame obj
            DontDestroyOnLoad(gameObject);
            savegame = this;
        }
        else if (savegame != this)
        {
            //If one is already in this scene then destroy the second (uneccessary) one
            Destroy(gameObject);
        }
        //Load();
    }

    public void Save()
    {
        BinaryFormatter bf = new BinaryFormatter();
        FileStream file = File.Create(Application.persistentDataPath + "/playerInfo.dat");

        //creating an instance of my PlayerData class
        PlayerData data = new PlayerData();

        data.gotScissors = gotScissors;
        data.collectedMeat = collectedMeat;
        bf.Serialize(file, data);
        file.Close();
    }

    public void Load()
    {
        if (File.Exists(Application.persistentDataPath + "/playerInfo.dat"))
        {
            BinaryFormatter bf = new BinaryFormatter();
            FileStream file = File.Open(Application.persistentDataPath + "/playerInfo.dat", FileMode.Open);
            PlayerData data = (PlayerData)bf.Deserialize(file);
            file.Close();

            gotScissors = data.gotScissors;
            collectedMeat = data.collectedMeat;
        }
    }

}

//Class with players saved values
[System.Serializable]
class PlayerData
{
    public bool collectedMeat;
    public bool gotScissors;
    public bool angeredCara;
}


