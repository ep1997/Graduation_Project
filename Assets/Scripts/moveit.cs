﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class moveit : MonoBehaviour
{
    Animator myAnimator;
    Animation myAnimation;

    void Start()
    {
        myAnimator = GetComponent<Animator>();
        myAnimation = GetComponent<Animation>();
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.LeftArrow) || (Input.GetKeyDown(KeyCode.RightArrow)))
        {
            if(myAnimator.GetBool("Moving") == true)
            {
                myAnimator.SetBool("Moving", false); 
            }
            else
            {
                myAnimator.SetBool("Moving", true);
            }
        }

        if (Input.GetKeyUp(KeyCode.LeftArrow) || (Input.GetKeyUp(KeyCode.RightArrow)))
        {
            if (myAnimator.GetBool("Moving") == false)
            {
                myAnimator.SetBool("Moving", true);
            }
            else
            {
                myAnimator.SetBool("Moving", false);
            }
        }

        if (Input.GetKeyDown(KeyCode.Space))
        {
            if (myAnimator.GetBool("Jumping") == true)
            {
                myAnimator.SetBool("Jumping", false);
            }
            else
            {
                myAnimator.SetBool("Jumping", true);
            }
        }

        if (Input.GetKeyUp(KeyCode.Space))
        {
            if (myAnimator.GetBool("Jumping") == false)
            {
                myAnimator.SetBool("Jumping", true);
            }
            else
            {
                myAnimator.SetBool("Jumping", false);
            }
        }

    }

}
