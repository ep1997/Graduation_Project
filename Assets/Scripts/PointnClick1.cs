﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PointnClick1 : MonoBehaviour
{

    public GameObject cube; //the red cube, should appear where I click
    Vector3 targetPosition; //the target for the cube

    void Start()
    {

        targetPosition = transform.position; //targetposition equals the current position of the gameobject
    }
    void Update()
    {
        
        if (Input.GetMouseButtonDown(0)) //When left mousebutton is pushed
        {
            
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition); //Cast a raycast through the MainCamera
            RaycastHit hit;

            if (Physics.Raycast(ray, out hit)) //if the raycast hit something on the plane
            {
                targetPosition = hit.point; //the targetposition will be whereever I clicked
                cube.transform.position = targetPosition; //So the cube will be teleported to the targetposition
            }
        }
    }
}
